import {TSESTree, TSESLint} from "@typescript-eslint/utils";
import {createRule} from "../../utils/createRule";
// import {typedTokenHelpers} from "../../utils/typedTokenHelpers";

export const allAsyncMethodsShouldPrefixWithAsync = (node: TSESTree.MethodDefinition) => {

    function findClassDeclaration(
        node: TSESTree.Node
    ): TSESTree.ClassDeclaration | null {
        if (node.type === "ClassDeclaration") {
            return node;
        }
        if (node.parent) {
            return findClassDeclaration(node.parent);
        }
        return null;
    }

    const classNode = findClassDeclaration(node);


};

const rule = createRule({
    name: "async-methods-should-postfix-with-async",
    meta: {
        docs: {
            description: "All async methods should be postfixed with async",
            recommended: false,
            requiresTypeChecking: false,
        },
        messages: {
            allAsyncMethodsShouldPrefixWithAsync:
                "All async methods should be postfixed with async",
        },
        schema: [],
        hasSuggestions: false,
        type: "suggestion",
    },
    defaultOptions: [],
    create(
        context: Readonly<
            TSESLint.RuleContext<"allAsyncMethodsShouldPosfixWithAsync", never[]>
        >
    ) {
        return {
            // eslint-disable-next-line @typescript-eslint/naming-convention
            MethodDefinition(node: TSESTree.MethodDefinition): void {


            },
        };
    },
});

export default rule;
